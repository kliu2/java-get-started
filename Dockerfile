FROM tomcat:9.0.24-jdk11-openjdk-slim
COPY target/dogwar-1.0.war /usr/local/tomcat/webapps/dog.war
EXPOSE 8080